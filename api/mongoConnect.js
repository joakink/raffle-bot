import { MongoClient } from 'mongodb';

const { DB_USER, DB_PASS, DB_HOST } = process.env;

const mongoConnect = () => new Promise((resolve, reject) => {
  const uri = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}?retryWrites=true&w=majority`;

  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  client.connect(err => {
    if (err) {
      console.log(`error connecting to ${DB_USER} : ${DB_PASS}`);
      client.close();
      reject(err);
    } else {
      resolve(client);
    }
  });
});

export default mongoConnect;
