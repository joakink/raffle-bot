import { NowRequest, NowResponse } from '@vercel/node';
import axios from 'axios';
import { ObjectId } from 'bson';

import mongoConnect from './mongoConnect';

const transfer = async ({ userId, amount, concept }) => {
  try {
    await axios({
      url: 'https://production.mazmoapi.net/bank/transactions',
      method: 'POST',
      params: { botSecret: process.env.SECRET },
      data: {
        to: { type: 'USER', id: parseInt(userId, 10) },
        concept,
        amount,
        data: {},
      },
    });
  } catch (error) {
    console.log('error', error);
  }
};

export default async (req: NowRequest, res: NowResponse) => {
  const { changeEvent: { fullDocument: { _id } }, secret } = JSON.parse(req.body) || {};

  if (secret !== process.env.SECRET) {
    return res.status(403).send('FORBIDDEN');
  }


  const client = await mongoConnect();
  const games = client.db('raffle').collection('games');
  const game = await games.findOne({ _id: ObjectId(_id) });


  const post = async (data) => {
    await client.close();

    await axios({
      url: `https://production.mazmoapi.net/chat/channels/${game.channelId}/messages`,
      method: 'POST',
      headers: { 'Bot-Key': game.key },
      data,
    });

    return res.status(200).send('OK');
  };

  if (!game || !game.participants || !game.participants.length) {
    if (game) {
      await games.updateOne({ _id: ObjectId(game._id) }, { $set: { finished: true } });
    }

    return post({ rawContent: '**¡Se acabó el tiempo!** Pero nadie quiso jugar :(' });
  }

  const candidates = game.participants;
  const winner = candidates[Math.floor(Math.random() * candidates.length)];
  const { data: users } = await axios.get(`https://production.mazmoapi.net/users?ids=${winner}`);


  const user = users[winner];
  let wording = 'Le ganadore es ';
  if (user.pronoun === 'MALE') wording = 'El ganador es';
  if (user.pronoun === 'FEMALE') wording = 'La ganadora es';

  await games.updateOne({ _id: ObjectId(game._id) }, { $set: { finished: true, winner } });

  const amount = (game.participants ? game.participants.length : 0) * game.amount;
  await transfer({ userId: winner, amount, concept: 'Ganaste el juego Raffle' });

  return post({ rawContent: `¡Se acabó el tiempo!\n\n:moneybag::moneybag::moneybag::moneybag::moneybag::moneybag::moneybag::moneybag:\n**${wording} @${user.username}**\n:moneybag::moneybag::moneybag::moneybag::moneybag::moneybag::moneybag::moneybag:` });
}
