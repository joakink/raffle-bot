import { NowRequest, NowResponse } from '@vercel/node';
import axios from 'axios';

import mongoConnect from './mongoConnect';

const OWNER_ID = 2;

export default async (req: NowRequest, res: NowResponse) => {
  const { message, key } = req.body || {};

  if (req.headers['bot-secret'] !== process.env.SECRET) return res.status(403).send('FORBIDDEN');
  if (!message || !key) return res.status(400).send('No message or key');

  const parts = message.payload.rawContent.split(' ');
  if (parts[0] !== '/raffle') return res.status(204).send('Nothing to do');

  const post = async (data) => {
    return axios({
      url: `https://production.mazmoapi.net/chat/channels/${message.channel.id}/messages`,
      method: 'POST',
      headers: { 'Bot-Key': key },
      data,
    });
  };

  if (parts[1] === 'balance' && message.author.id === OWNER_ID) {
    // Owner can now balance
    const { data: { balance } } = await axios({
      url: `https://production.mazmoapi.net/bank/boxes/balance`,
      method: 'GET',
      params: { botSecret: process.env.SECRET },
    });

    await post({
      rawContent: `El balance en cuenta es de **${balance} sades**.`,
      type: 'NOTICE',
      toUserId: 2,
    });

    return res.status(200).send('OK');
  }

  if (parts[1] === 'drain' && message.author.id === OWNER_ID) {
    // Owner can transfer balance to self
    const { data: { balance } } = await axios({
      url: `https://production.mazmoapi.net/bank/boxes/balance`,
      method: 'GET',
      params: { botSecret: process.env.SECRET },
    });

    if (!balance) {
      await post({
        rawContent: `Estoy como el chavo, no tengo una moneda para transferirte :disappointed:`,
        type: 'NOTICE',
        toUserId: 2,
      });

      return res.status(200).send('OK');
    }

    await axios({
      url: 'https://production.mazmoapi.net/bank/transactions',
      method: 'POST',
      params: { botSecret: process.env.SECRET },
      data: {
        to: { type: 'USER', id: OWNER_ID },
        concept: 'Raffle drain',
        amount: balance,
      },
    });

    await post({
      rawContent: `Se transfirieron todos los fondos (${balance} sades) a tu cuenta :money_mouth_face:`,
      type: 'NOTICE',
      toUserId: 2,
    });

    return res.status(200).send('OK');
  }

  const client = await mongoConnect();
  const games = client.db('raffle').collection('games');

  const send = async (body, code = 200) => {
    await client.close();
    return res.status(code).send(body);
  };

  const sades = parseFloat(parts[1]) || 10;

  if (Number.isNaN(sades) || sades < 0.1) {
    await post({ rawContent: 'No te hagas el vivo' });
    return send('OK');
  }

  const existingGame = await games.findOne({
    channelId: message.channel.id,
    finished: false,
  });

  if (!existingGame) {
    const { insertedId } = await games.insertOne({
      channelId: message.channel.id,
      key,
      createdAt: new Date(),
      createdBy: message.author.id,
      amount: sades,
      finished: false,
    });

    await post({
      rawContent: `¡Juego comenzado! Para participar, transferir ${sades} sades.\n\nTienen 30 segundos para transferir y jugar.`,
      sadesAsk: {
        amount: sades,
        fixed: true,
        transferData: { gameId: insertedId },
      },
    });

    return send('OK')
  }

  await post({
    rawContent: `Ya existe una partida activa. Unite o esperá a la próxima`,
    type: 'NOTICE',
    toUserId: message.author.id,
  });

  return send('OK')
}
