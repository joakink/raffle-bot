import { NowRequest, NowResponse } from '@vercel/node';
import axios from 'axios';
import { ObjectId } from 'bson';

import mongoConnect from './mongoConnect';

export default async (req: NowRequest, res: NowResponse) => {
  const { transaction } = req.body || {};

  const client = await mongoConnect();
  const games = client.db('raffle').collection('games');

  const game = await games.findOne({ _id: ObjectId(transaction.data.gameId) });

  if (!game) {
    return res.status(200).send('No game found');
  }

  const post = async (data) => {
    await client.close();

    await axios({
      url: `https://production.mazmoapi.net/chat/channels/${game.channelId}/messages`,
      method: 'POST',
      headers: { 'Bot-Key': game.key },
      data,
    });

    return res.status(200).send('OK');
  };

  if (game.finished) {
    return post({ type: 'NOTICE', toUserId: transaction.from.owner.id, rawContent: 'Enviaste sades para un juego que ya terminó.' });
  }
  if (transaction.amount !== game.amount) {
    return post({ type: 'NOTICE', toUserId: transaction.from.owner.id, rawContent: 'Enviaste una suma diferente a la requerida por el juego.' });
  }

  await games.updateOne({ _id: ObjectId(transaction.data.gameId) }, { $push: { participants: transaction.from.owner.id } });

  const { data: users } = await axios.get(`https://production.mazmoapi.net/users?ids=${transaction.from.owner.id}`);
  const user = users[transaction.from.owner.id];
  const participants = (game.participants ? game.participants.length : 0) + 1;

  return post({ rawContent: `@${user.username} puso la tarasca y está compitiendo por ${participants * game.amount} sades :sunglasses:` });
}
