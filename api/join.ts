import { NowRequest, NowResponse } from '@vercel/node';
import axios from 'axios';

export default async (req: NowRequest, res: NowResponse) => {
  if (req.headers['bot-secret'] !== process.env.SECRET) return res.status(403).send('FORBIDDEN');

  const { message, key } = req.body || {};

  if (!message || !key) return res.status(400).send('No message or key');

  const rawContent = '¡Holis!';
  await axios({
    url: `https://production.mazmoapi.net/chat/channels/${message.channel.id}/messages`,
    method: 'POST',
    headers: { 'Bot-Key': key },
    data: { rawContent },
  });

  return res.status(200).send('OK');
}
